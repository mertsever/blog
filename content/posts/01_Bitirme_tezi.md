---
author:
  name: "Mert SEVER"
date: 2018-05-22
linktitle: Bitirme
type:
- post
- posts
title: Rocket Propulsion Analysis Program Optimization
weight: 10
series:
- Hugo 101
---
  Rocket Propulsion Analysis (RPA) is a program that simulates chamber conditions. In this study, an example conditions
has used for analysis. With using that example conditions, ISp has optimized by using modeFrontier program and brute force algorithm. As a result of this study, its expressed that a maximum or minimum value of selected state of chamber conditions can be found without trying all probabilities. Chosen output variable can be optimized due to two other input variable.

[RPA Optimization](/bitirme_tezi_revision.pdf)


```
