---
author:
  name: "Mert SEVER"
date: 2021-08-31
linktitle: GNSS Signal Processing with EKF and UKF for Stationary User Position Estimation
type:
- post
- posts
title: GNSS Signal Processing with EKF and UKF for Stationary User Position Estimation
weight: 10
series:
- Hugo 101
---
Precise and accurate estimation of state vectors is an important process during position 
determination. In this study, Extended Kalman Filter (EKF) and Unscented Kalman Filter (UKF) of stationary 
user, state vectors defined in Earth Centered Inertial (ECI) coordinate system, accompanied by GNSS 
measurement data. It is aimed to make estimations with methods. EKF and UKF methods were compared with 
each other. In this study, the effects of nonlinear motion analysis and linearization methods on state vector 
estimations were investigated. Thanks to this study, estimations of the positioning information required during 
the specific tasks of many moving platforms have been made.

[GNSS Signal Processing with EKF and UKF for Stationary User Position Estimation](/https://www.researchgate.net/publication/353718126_GNSS_Signal_Processing_with_EKF_and_UKF_for_Stationary_User_Position_Estimation)


```
