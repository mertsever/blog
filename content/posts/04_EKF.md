---
author:
  name: "Mert SEVER"
date: 2020-06-25
linktitle: EKF
type:
- post
- posts
title: Extended Kalman Filter for Satellite Localization
weight: 10
series:
- Hugo 101
---
  Extended Kalman Filter (EKF) is an mathematical approach of optimization. In this study, satellite localization is simulated. The satellite's position has determined by Kepler equations. With using random measurement errors which were obtained from gps reciever, the satellite's position and velocity state vectors obtained. These state vectors optimized with EKF approach and more accurate model has developed.

[EKF](/EKF.pdf)


```
