---
author:
  name: "Mert SEVER"
date: 2019-12-29
linktitle: Spacecraft Simulation Softwares
type:
- post
- posts
title: Spacecraft Simulation Softwares
weight: 10
series:
- Hugo 101
---
  General Mission Analysis Tool (GMAT) and Systems Tool Kit (STK) are softwares for simulating spacecraft's mission. In this study these two programs introduced for new users. Also you can download these programs from links below.

[Spacecraft Simulation Sowtwares](/seminer.pdf)

[STK](/https://licensing.agi.com/stk/)
[GMAT](https://software.nasa.gov/software/GSC-17177-1)



```
