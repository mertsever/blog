---
author:
  name: "Mert SEVER"
date: 2020-01-06
linktitle: PSO
type:
- post
- posts
title: Particle Swarm Optimization
weight: 10
series:
- Hugo 101
---
  Particle swarm optimization (PSO) is an optimization algorithm developed in 1995 by Dr. Kennedy and Dr. Eberhart, inspired by the observation that the movements of some animals moving in the herd, while supplying their basic needs to find food, affect other individuals in the herd and achieve the purpose of the herd more easily. In this study, Particle swarm optimization method explained with an example.

[Particle Swarm Optimization](/PSO.pdf)

This algorithm applied to Rocket Propulsion Analysis program to optimize Isp (vac) output by Atacan KULLABCI with using JAVA. You can visit his website for more applications.
[PSO Java Code](/PSO_CODE.txt)
[Atacan KULLABCI](http://www.atacankullabci.com)

```
