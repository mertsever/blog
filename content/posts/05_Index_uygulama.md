---
author:
  name: "Mert SEVER"
date: 2020-06-27
linktitle: Index uygulaması
type:
- post
- posts
title: Comparision of Two Satellite Image by Using Index Application
weight: 10
series:
- Hugo 101
---
Remote sensing is a process of detecting and monitoring the meteorology, Earth and ground resources without establishing physical contact. In this study, two LANDSAT-8 images were used for determining the change in specified area.NDVI, SAVI, NDWI and NDTI indexes has chosen for analysis. Its expressed that, change in ground can be easily determined by using index application. 

[Remote Sensing Index Application](/index_uygulamasi_rapor.pdf)


```