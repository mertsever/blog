+++
title = "Mert SEVER"
date = "2020-08-25"
aliases = ["about-us","about-hugo","contact"]
[ author ]
  name = "Mert SEVER"
+++
An astronautical engineer who has an enthusiasm about orbital mechanics, spacecraft mission planning and attitude determination and control.

## Contact
```
Etimesgut,Ankara, TURKEY
E-mail: mert.sever@windowslive.com
```
Please check following documents for more information.

[CV English](/mert_sever_cv.pdf)
[CV Turkish](/mert_sever_ozgecmis.pdf)
[Bachelor of Science Transcript](/lisans_transkript.pdf)
[Master of Science Transcript](/yl_transkript.pdf)